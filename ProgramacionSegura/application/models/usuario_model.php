<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	public function listarPersona()
	{
		$this->db->select('usuario.id_Usuario,persona.nombres,persona.apellidos,persona.genero,usuario.nombreUsuario,usuario.rol,usuario.estado');
		$this->db->from('persona');
		$this->db->join('usuario','persona.id_Persona = usuario.id_Usuario');
		return $this->db->get();
	}

public function listarRoles($id)
	{
		$this->db->select('rol.id_rol,rol.rol');
		$this->db->from('rol_has_usuario');
		$this->db->join('rol','rol_has_usuario.id_rol = rol.id_rol');
		$this->db->join('usuario','rol_has_usuario.id_Usuario = usuario.id_Usuario');
		$this->db->where('usuario.id_Usuario',$id);

		return $this->db->get();
	}
	
	
	
	public function LoginSystem($nombreUsuario,$password)
	{

	
		
		$this->db->select('id_Usuario,nombreUsuario,password,rol');
		$this->db->from('usuario');
		
		$this->db->where('nombreUsuario',$nombreUsuario)->where('password',$password)->where('estado','1');
		return $this->db->get();
		
	}

	public function agregarUsuario($data,$data2)
	{
		$this->db->trans_begin();
		$this->db->insert('persona',$data);
		$insert_id=$this->db->insert_id();
		$data2['id_Usuario'] = $insert_id;
		$this->db->insert('usuario',$data2);
		if ($this->db->trans_status()== FALSE) {
			$this->db->trans_rollback();
		}
		else
		{
				$this->db->trans_commit();
		}
	}

	public function recuperarusuario($idusuario)
	{
		$this->db->select('usuario.id_Usuario,persona.nombres,persona.apellidos,persona.fecha_nacimiento,persona.genero,usuario.nombreUsuario,usuario.password,usuario.rol,usuario.estado');
		$this->db->from('persona');
		$this->db->join('usuario','persona.id_Persona = usuario.id_Usuario');
		$this->db->where('usuario.id_Usuario',$idusuario);
		return $this->db->get();
	}

	public function modificarUsuario($idusuario,$data,$data2)
	{
		$this->db->trans_begin();
		$this->db->where('id_Persona',$idusuario);
		$this->db->update('persona',$data);

		$this->db->where('id_Usuario',$idusuario);
		$this->db->update('usuario',$data2);

		if ($this->db->trans_status()== FALSE) {
			$this->db->trans_rollback();
		}
		else
		{
				$this->db->trans_commit();
		}
	}

	public function eliminarbdd($idusuario)
	{
		$this->db->where('id_Usuario',$idusuario);
		$this->db->delete('usuario');
	}

	public function activar_inactivar($idusuario,$data)
	{
		$this->db->where('id_Usuario',$idusuario);
		$this->db->update('usuario',$data);
	}
}
