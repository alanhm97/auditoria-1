<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tema_model extends CI_Model {


	public function GetTemaAvanceUsuario($idusuario)
	{
		$this->db->select('avance_tema_usuario.a_idTema, tema.titulo,tema.descripcion,categoria.nombre_categoria,avance_tema_usuario.estado_avance,avance_tema_usuario.porcentaje');
		$this->db->from('tema');
		$this->db->join('avance_tema_usuario','tema.idtema = avance_tema_usuario.a_idTema');
		$this->db->join('categoria','categoria.idcategoria = tema.id_categoria');
		$this->db->where('avance_tema_usuario.a_id_Usuario',$idusuario);
		return $this->db->get();
	}

	public function listarItems()
	{
		$this->db->select('CATEGORIA.IDCATEGORIA,CATEGORIA.NOMBRE_CATEGORIA,TEMA.IDTEMA,TEMA.TITULO,TEMA.DESCRIPCION');
		$this->db->from('CATEGORIA');
		$this->db->join('TEMA','CATEGORIA.IDCATEGORIA = TEMA.ID_CATEGORIA');
		$this->db->order_by("CATEGORIA.NOMBRE_CATEGORIA", "asc");
		return $this->db->get();
	}

	
	public function listarItemsWhere($categoria)
	{
		$this->db->select('CATEGORIA.IDCATEGORIA,CATEGORIA.NOMBRE_CATEGORIA,TEMA.IDTEMA,TEMA.TITULO,TEMA.DESCRIPCION');
		$this->db->from('CATEGORIA');
		$this->db->join('TEMA','CATEGORIA.IDCATEGORIA = TEMA.ID_CATEGORIA');
		$this->db->where('CATEGORIA.NOMBRE_CATEGORIA',$categoria);
		$this->db->order_by("CATEGORIA.NOMBRE_CATEGORIA", "asc");
		return $this->db->get();
	}

	// tema categoria
	public function listarItemsTema($idTema)
	{
		$this->db->select('CATEGORIA.IDCATEGORIA,CATEGORIA.NOMBRE_CATEGORIA,TEMA.IDTEMA,TEMA.TITULO,TEMA.DESCRIPCION');
		$this->db->from('CATEGORIA');
		$this->db->join('TEMA','CATEGORIA.IDCATEGORIA = TEMA.ID_CATEGORIA');
		$this->db->where('TEMA.IDTEMA',$idTema);
	
		return $this->db->get();
	}

	// tema / subtitulo / descripcion subtitulo
	public function listarTemaSubtituloDescripcion($idTema)
	{
		$this->db->select('TEMA.IDTEMA,TEXTO.IDTEXTO,TEXTO.SUBTITULO,TEXTO.TEXTO');
		$this->db->from('TEMA');
		$this->db->join('TEMATEXTO','TEMATEXTO.TEMA_IDTEMA = TEMA.IDTEMA');
		$this->db->join('TEXTO','TEXTO.IDTEXTO = TEMATEXTO.TEXTO_IDTEXTO');
		$this->db->where('TEMATEXTO.TEMA_IDTEMA',$idTema);
		return $this->db->get();
	}

	//tema imagen
	
	public function listarTemaImagenes($idTema)
	{
		$this->db->select('TEMA.IDTEMA,IMAGEN.IDIMAGEN,IMAGEN.DIRECCIONIMAGEN');
		$this->db->from('TEMA');
		$this->db->join('TEMAIMAGEN','TEMAIMAGEN.TEMA_IDTEMA = TEMA.IDTEMA');
		$this->db->join('IMAGEN','IMAGEN.IDIMAGEN = TEMAIMAGEN.IMAGEN_IDIMAGEN');
		$this->db->where('TEMA.IDTEMA',$idTema);
		return $this->db->get();
	}

	//TEMA AUDIO
	public function ListarTemaCanciones($idTema)
	{
		$this->db->select('TEMA.IDTEMA,AUDIO.IDAUDIO,AUDIO.DIRECCIONAUDIO');
		$this->db->from('TEMA');
		$this->db->join('TEMAAUDIO','TEMAAUDIO.TEMA_IDTEMA = TEMA.IDTEMA');
		$this->db->join('AUDIO','AUDIO.IDAUDIO = TEMAAUDIO.AUDIO_IDAUDIO');
		$this->db->where('TEMA.IDTEMA',$idTema);
		return $this->db->get();
	}


	public function recuperarusuario($idusuario)
	{
		$this->db->select('usuario.id_Usuario,persona.nombres,persona.apellidos,persona.fecha_nacimiento,persona.genero,usuario.nombreUsuario,usuario.password,usuario.estado');
		$this->db->from('persona');
		$this->db->join('usuario','persona.id_Persona = usuario.id_Usuario');
		$this->db->where('usuario.id_Usuario',$idusuario);
		return $this->db->get();
	}






 
	public function GetInfoTema($idTema)
	{
		$this->db->select('t.idTema,t.titulo,t.descripcion, tx.texto, tx.subtitulo, c.idcategoria,c.nombre_categoria');
		$this->db->from('tema as t');
		$this->db->join('categoria as c','c.idcategoria= t.id_categoria');
		$this->db->join('tematexto as tt','t.idTema=tt.Tema_idTema');
		$this->db->join('texto as tx','tt.Texto_idTexto = tx.idTexto','left');
		$this->db->where('t.idTema',$idTema);
		return $this->db->get();

	}


 public function EliminarTema($idTema){
    $this->db->trans_begin();

    $this->db->where('Tema_idTema',$idTema);
    $this->db->delete('tematexto');

 	$this->db->where('Tema_idTema',$idTema);
    $this->db->delete('temaimagen');

    $this->db->where('Tema_idTema',$idTema);
    $this->db->delete('temaaudio');

    $this->db->where('idTema',$idTema);
    $this->db->delete('tema');

    if ($this->db->trans_status()== FALSE) {
      $this->db->trans_rollback();
    }
    else
    {
        $this->db->trans_commit();
    }
    }




 private function set_upload_options($name,$dir)
    {   
        //upload an image options
        $config = array();
       // $config['upload_path']=base_url()."assets/img/";
        $config['upload_path']=$dir;
        
    	$config['allowed_types']='jpg|gif|png|jpeg|JPG|PNG|mp3|ogg';    
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;
        $config['file_name']= $name;        

        return $config;
    }

	public function SubirTemaDB($data,$data2,$data3)
	{
		$this->db->trans_begin();
		try {

			$this->db->insert('tema',$data);
		$idTema=$this->db->insert_id();
		
		$dataTexto;
		$dataTexto["texto"]=$data2["texto"];
		$dataTexto["subtitulo"]=$data2["sub"];
		$this->db->insert('texto',$dataTexto);
		$idTexto=$this->db->insert_id();

		$dataTT;
		$dataTT["Tema_idTema"]=$idTema;
		$dataTT["Texto_idTexto"]=$idTexto;
		$this->db->insert('tematexto',$dataTT);

		$dataTI;
		$dataTI["Tema_idTema"]=$idTema;

		$dataTA;
		$dataTA["Tema_idTema"]=$idTema;

		$this->load->library('upload');

        
        $cpt = count($data3['files']['name']);
        //echo "count: ".$cpt;
       
        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['files']['name']= $data3['files']['name'][$i];
            $_FILES['files']['type']= $data3['files']['type'][$i];
           $_FILES['files']['tmp_name']= $data3['files']['tmp_name'][$i];
            $_FILES['files']['error']= $data3['files']['error'][$i];
           $_FILES['files']['size']= $data3['files']['size'][$i];

           $path=pathinfo($data3['files']['name'][$i], PATHINFO_EXTENSION); 
            $nameFile=time();   
			 if ($path=='jpg' || $path=='jpeg' || $path=='gif' || $path=='png') {
				$dir='./assets/img/';
			}
			else if($path=='mp3' || $path=='ogg')
				{
					$dir='./assets/audio/';
				}
            $this->upload->initialize($this->set_upload_options($nameFile,$dir));
           // $this->upload->do_upload();
        
       
        if ( ! $this->upload->do_upload('files')) {
          // echo '<pre>';
          // print_r($this->upload->display_errors());
        	throw new Exception("Error Processing Request", 1);
        	
          
       } else {
          // echo '<pre>';
         //  print_r($this->upload->data());
           

           if ($path=='mp3' || $path=='ogg') {
           	$dataAudio;
           	$dataAudio["direccionAudio"]=$nameFile.'.'.$path;
           	$this->db->insert('audio',$dataAudio);
           	$idAudio=$this->db->insert_id();
           	$dataTA["Audio_idAudio"]=$idAudio;
			$this->db->insert('temaaudio',$dataTA);
           }
           else if ($path=='jpg' || $path=='jpeg' || $path=='gif' || $path=='png') {
			$dataImagen;
           	$dataImagen["direccionImagen"]=$nameFile.'.'.$path;
           	$this->db->insert('Imagen',$dataImagen);
           	$idImagen=$this->db->insert_id();
           	$dataTI["Imagen_idImagen"]=$idImagen;
			$this->db->insert('temaImagen',$dataTI);
           }
           
       }
	}
		
		
		if ($this->db->trans_status()== FALSE) {
			$this->db->trans_rollback();
			echo "Ocurrio un error al procesar la petición.";
		}
		else
		{
				$this->db->trans_commit();
				//echo "<br><h2>Subido</h2>";
		}
		
	}
			
		 catch (Exception $e) {
			$this->db->trans_rollback();
			echo "Ocurrio un error";
		}
		
		

}

	public function GetImagenesEdit($id)
	{


		$this->db->select('idImagen,direccionImagen');
		$this->db->from('imagen');
		$this->db->join('temaimagen','idImagen=Imagen_idImagen');
		$this->db->where('Tema_idTema',$id);
		return $this->db->get();
		
	}

	public function GetAudiosEdit($id)
	{


		$this->db->select('idAudio,direccionAudio');
		$this->db->from('audio');
		$this->db->join('temaaudio','idAudio=Audio_idAudio');
		$this->db->where('Tema_idTema',$id);
		return $this->db->get();
		
	}


	public function QuitarImagenTemaBD($idImagen,$idTema)
	{


		 $this->db->where('Tema_idTema',$idTema);
		 $this->db->where('Imagen_idImagen',$idImagen);
		$this->db->delete('temaimagen');
	}

	public function QuitarAudioTemaBD($idAudio,$idTema)
	{


		 $this->db->where('Tema_idTema',$idTema);
		 $this->db->where('Audio_idAudio',$idAudio);
		$this->db->delete('temaaudio');
	}

	public function GetCategorias()
	{
		$this->db->select('*');
		$this->db->from('categoria');
		return $this->db->get();
	}


	function GetIDTexto($idTema)
	{
		$this->db->select('Texto_idTexto');
		$this->db->from('tematexto');
		$this->db->where('tema_idtema',$idTema);
		return $this->db->get()->row()->Texto_idTexto;
	}

	public function EditarTemaDB($data,$data2,$data3)
	{
		$this->db->trans_begin();
		try {

		$idTema=$data['idTema'];
		

		$this->db->where('idTema',$idTema);
		$this->db->update('tema',$data);
		
		
		$dataTexto;
		$dataTexto["texto"]=$data2["texto"];
		$dataTexto["subtitulo"]=$data2["sub"];
		$this->db->where('idTexto',$this->GetIDTexto($idTema));
		$this->db->update('texto',$dataTexto);
		//$idTexto=$this->db->insert_id();

		//$dataTT;
		//$dataTT["Tema_idTema"]=$idTema;
		//$dataTT["Texto_idTexto"]=$idTexto;
		//$this->db->insert('tematexto',$dataTT);

		$dataTI;
		$dataTI["Tema_idTema"]=$idTema;

		$dataTA;
		$dataTA["Tema_idTema"]=$idTema;

		$this->load->library('upload');

        
        $cpt = count($data3['files']['name']);
        //echo "count: ".$cpt;
      // echo $data3['files']['name'];
       $dir='./assets/img/';
        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['files']['name']= $data3['files']['name'][$i];
            $_FILES['files']['type']= $data3['files']['type'][$i];
           $_FILES['files']['tmp_name']= $data3['files']['tmp_name'][$i];
            $_FILES['files']['error']= $data3['files']['error'][$i];
           $_FILES['files']['size']= $data3['files']['size'][$i];

           $path=pathinfo($data3['files']['name'][$i], PATHINFO_EXTENSION); 
            $nameFile=time();   
			 if ($path=='jpg' || $path=='jpeg' || $path=='gif' || $path=='png') {
				$dir='./assets/img/';
			}
			else if($path=='mp3' || $path=='ogg')
				{
					$dir='./assets/audio/';
				}
            $this->upload->initialize($this->set_upload_options($nameFile,$dir));
           // $this->upload->do_upload();
        
       
        if ( ! $this->upload->do_upload('files')) {
          // echo '<pre>';
          // print_r($this->upload->display_errors());
        	//!!!throw new Exception("Error Processing Request", 1);
        	
          
       } else {
          // echo '<pre>';
         //  print_r($this->upload->data());
           

           if ($path=='mp3' || $path=='ogg') {
           	$dataAudio;
           	$dataAudio["direccionAudio"]=$nameFile.'.'.$path;
           	$this->db->insert('audio',$dataAudio);
           	$idAudio=$this->db->insert_id();
           	$dataTA["Audio_idAudio"]=$idAudio;
			$this->db->insert('temaaudio',$dataTA);
           }
           else if ($path=='jpg' || $path=='jpeg' || $path=='gif' || $path=='png') {
			$dataImagen;
           	$dataImagen["direccionImagen"]=$nameFile.'.'.$path;
           	$this->db->insert('Imagen',$dataImagen);
           	$idImagen=$this->db->insert_id();
           	$dataTI["Imagen_idImagen"]=$idImagen;
			$this->db->insert('temaImagen',$dataTI);
           }
           
       }
	}
		
		
		if ($this->db->trans_status()== FALSE) {
			$this->db->trans_rollback();
			echo "Ocurrio un error al procesar la petición.";
		}
		else
		{
				$this->db->trans_commit();
				//echo "<br><h2>Subido</h2>";
		}
		
	}
			
		 catch (Exception $e) {
			$this->db->trans_rollback();
			echo "Ocurrio un error ";
			echo $e->getMessage();
		}
		
		

}


		public function AnhadirVisita($idTema)
		{

			$this->db->set('visitas', 'visitas+1', FALSE);
			$this->db->where('idTema', $idTema);
			$this->db->update('tema');

		}



}





?>
