<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('usuario_model');
		$this->load->model('tema_model');
	}

	public function principal()
	{
		$listarItem=$this->tema_model->listarItems();
		$data['items']=$listarItem;
		$this->load->view('inc/template_header');
		$this->load->view('view_user',$data);
		$this->load->view('inc/template_footer');
	}

	
	
	public function index()
	{
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/view_login');
		$this->load->view('inc/template_footer');
	}

	public function AddUser()
	{
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/view_loginadd');
		$this->load->view('inc/template_footer');
	}
	
	public function LoginSystem()
	{
		
		$login=$_POST['usuario'];
		$password=md5($_POST['password']);

		$consulta = $this->usuario_model->LoginSystem($login,$password);

		if ($consulta->num_rows()>0) {
			foreach ($consulta->result() as $row)
	        {
	          if(($row->nombreUsuario)==$login && ($row->password)==$password)
	          {
	          	$this->session->set_userdata('id_Usuario',$row->id_Usuario);
                $this->session->set_userdata('nombreUsuario',$row->nombreUsuario);
                $this->session->set_userdata('rol',$row->rol);
                redirect('usuarios/vistausuario','refresh');
	          }
	      	}
		}
		else{
			redirect('usuarios','refresh');
		}
		
	}

	public function logout()
    {
        $this->session->sess_destroy();
        redirect('usuarios','refresh');
	}
	
	public function vistausuario()
	{
		
	$listarRoles=$this->usuario_model->listarRoles($this->session->userdata('id_Usuario'));
		
		$data['roles']=$listarRoles;
		
		if($this->session->userdata('nombreUsuario'))
            {
				$this->load->view('inc/template_header');
				$this->load->view('view_user',$data);
				$this->load->view('inc/template_footer');

            }
        else
            {
            	redirect('usuario','refresh');
            }
	}

	public function listarPersonaUsuarios()
	{
		$listarItem=$this->tema_model->listarItems();
		$listarPersonaUsuarios=$this->usuario_model->listarPersona();
		$data['items']=$listarItem;
		$data['usuarios']=$listarPersonaUsuarios;
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/listaUsuarios',$data);
		$this->load->view('inc/template_footer');
	}


	public function agregar()
	{
		$listarItem=$this->tema_model->listarItems();
		$data['items']=$listarItem;
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/addUsuarios',$data);
		$this->load->view('inc/template_footer');
	}

	public function agregarBd()
	{
		$data['nombres']=$_POST['nombres'];
		$data['apellidos']=$_POST['apellidos'];
		$data['fecha_nacimiento']=$_POST['fechaNacimiento'];
		$data['pais_idpais']=1;
		
		$genero = $_POST['genero'];
		if ($genero == 'Masculino') {
			$data['genero']=1;
		}
		else
		{

			$data['genero']=0;
		}
		
		$data2['nombreUsuario']=$_POST['usuario'];
		$data2['password']=md5($_POST['password']);
		
		$data2['rol']=$_POST['roles'];
		

		$this->usuario_model->agregarUsuario($data,$data2);
		redirect('usuarios/index','refresh');
	}
	public function modificar()
	{
		
		$listarItem=$this->tema_model->listarItems();
		$data['items']=$listarItem;
		$idusuario=$_POST['idusuario'];
		$data['infousuario']=$this->usuario_model->recuperarusuario($idusuario);
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/updUsuarios',$data);
		$this->load->view('inc/template_footer');
	}

	public function ocultarbd()
	{
		$idusuario=$_POST['idusuario'];
		$data['estado']=0;
		$this->usuario_model->ocultarbd($idusuario,$data);
		redirect('usuarios/listarPersonaUsuarios','refresh');
	}

	public function eliminarbd()
	{
		$listarItem=$this->tema_model->listarItems();
		$data['items']=$listarItem;
		$idusuario=$_POST['idusuario'];
		$data['infousuario']=$this->usuario_model->recuperarusuario($idusuario);
		$this->load->view('inc/template_header');
		$this->load->view('vw_usuarios/delusuario',$data);
		$this->load->view('inc/template_footer');
		
	}

	public function eliminarbdd()
	{
		
		$idusuario=$_POST['idusuario'];
	
		$this->usuario_model->eliminarbdd($idusuario);
		redirect('usuarios/listarPersonaUsuarios','refresh');
		
	}

	public function activar_inactivar()
	{
		
		$idusuario=$_POST['idusuario'];
		$control = $_POST['control'];
		if ($control == 'Activo') {
			$data['estado'] = 0;
		}
		else{

			$data['estado'] = 1;
		}
	
		$this->usuario_model->activar_inactivar($idusuario,$data);
		redirect('usuarios/listarPersonaUsuarios','refresh');
		
	}

	public function modificarbd()
	{
		$idusuario=$_POST['idusuario'];
		$data['nombres']=$_POST['nombres'];
		$data['apellidos']=$_POST['apellidos'];
		$data['fecha_nacimiento']=$_POST['fechaNacimiento'];
		$data['pais_idpais']=1;

		$genero = $_POST['genero'];
		if ($genero == 'Masculino') {
			$data['genero']=1;
		}
		else
		{

			$data['genero']=0;
		}
		
		$data2['nombreUsuario']=$_POST['usuario'];
		$data2['password']=$_POST['password'];
		
		$data2['rol']=$_POST['roles'];

		$this->usuario_model->modificarUsuario($idusuario,$data,$data2);
		redirect('usuarios/listarPersonaUsuarios','refresh');
	}


}
