<!DOCTYPE html>
<html lang="en" class="no-ie">
<head>
   <!-- Meta-->
   <meta charset="utf-8">


   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/app/css/bootstrap.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/app/css/jquery-ui.css">
   <!-- Vendor CSS-->
   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/vendor/fontawesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/vendor/animo/animate+animo.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/vendor/csspinner/csspinner.min.css">
   <!-- START Page Custom CSS-->
   <!-- END Page Custom CSS-->
   <!-- App CSS-->
   <link rel="stylesheet" href="<?php echo base_url(); ?>47admin/app/css/app.css">
   <!-- Modernizr JS Script-->
   <script src="<?php echo base_url(); ?>47admin/vendor/modernizr/modernizr.js" type="application/javascript"></script>
   <!-- FastClick for mobiles-->
   <script src="<?php echo base_url(); ?>47admin/vendor/fastclick/fastclick.js" type="application/javascript"></script>
   
   <script src="<?php echo base_url(); ?>47admin/vendor/jquery/jquery.min.js"></script>
 <script src="<?php echo base_url(); ?>47admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>47admin/app/js/jquery-ui.js" type="application/javascript"></script>
   <!--  <script src="<?php echo base_url(); ?>47admin/app/js/jquery.min.js" type="application/javascript"></script> -->
</head>
<body>
   