<div style="height: 100%; padding: 50px 0; background-color: #2c3037" class="row row-table">
      <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
         <!-- START panel-->
         <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
           <!--  <p class="text-center mb-lg">
               <br>
               <a href="#">
                  <img src="<?php echo base_url();?>assets/img/logo.png" alt="Image" class="block-center img-rounded">
               </a>
            </p> -->
            <p class="text-center mb-lg">
               <!-- <strong>Aprende Quechua</strong> -->
            </p>
            <div class="panel-body">
            <form method="post"  action="<?php echo base_url();?>usuarios/LoginSystem" class="mb-lg">
                  <div class="text-right mb-sm"><a href="#" class="text-muted"></a>
                  </div>
                  <div class="form-group has-feedback">
                     <input name="usuario" type="text" placeholder="Usuario" class="form-control" required>
                     <span class="fa fa-envelope form-control-feedback text-muted"></span>
                  </div>
                  <div class="form-group has-feedback">
                     <input name="password" type="password" placeholder="Password" class="form-control" required>
                     <span class="fa fa-lock form-control-feedback text-muted"></span>
                  </div>
                 
                  <button type="submit" class="btn btn-block btn-primary">Iniciar Sesion</button></a>
               </form>
              <!--  <a href="<?php echo base_url(); ?>usuarios/AddUser"> <button type="submit" class="btn btn-block btn-primary">Registrar Cuenta</button></a> -->
            </div>
         </div>
         <!-- END panel-->
      </div>
   </div>
</div>