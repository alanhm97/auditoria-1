<div style="height: 100%; padding: 50px 0; background-color: #2c3037" class="row row-table">
      <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
         <!-- START panel-->
         <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
            <p class="text-center mb-lg">
               <br>
               
            </p>
            <p class="text-center mb-lg">
               <strong>QUECHUA</strong>
            </p>
            <div class="panel-body">
            <form method="post"  action="<?php echo base_url()?>usuarios/agregarBd">
                           <div class="form-group">
                              <label>Nombres</label>
                              <input type="text" name="nombres" placeholder="Ingresar Nombres" class="form-control" required>
                           </div>
                           <div class="form-group">
                              <label>Apellidos</label>
                              <input type="text" name="apellidos" placeholder="Ingresar Apellidos" class="form-control" required>
                           </div>
                           <div class="form-group">
                              <label>Fecha Nacimiento</label>
                              <input type="date"  name="fechaNacimiento" placeholder="Fecha Nacimiento" class="form-control" min="1960-1-1" max="2003-12-31" required>
                           </div>

                           <div class="form-group">
                              <label>Genero</label>
                              <select name="genero" class="form-control m-b" required>
                                 <option>Masculino</option>
                                 <option>Femenino</option>
                                
                                
                              </select>
                           </div>
                           <div class="form-group">
                              <label>Nombre de Usuario</label>
                              <input type="text" name="usuario" placeholder="Ingresar Nombre de Usuario" class="form-control" required>
                           </div>

                           <div class="form-group">
                              <label>Password</label>
                              <input type="password" name="password" placeholder="Ingresar Password" class="form-control" required>
                           </div>
                          
                           <div class="form-group">
                              <label>Rol de Usuario</label>
                               <input type="hidden"  name="roles" value="Estudiante"/>
                             
                                
                                
                           
                           </div>
                           <button type="submit" class="btn btn-sm btn-default">Registrar</button>
                        </form>
            </div>
         </div>
         <!-- END panel-->
      </div>
   </div>
</div>