<section class="wrapper">
      <!-- START Top Navbar-->
      <nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top">
         <!-- START navbar header-->
         <div class="navbar-header">
            <a href="#" class="navbar-brand">
               <div class="brand-logo">QUECHUA</div>
               <div class="brand-logo-collapsed">Q</div>
            </a>
         </div>
         <!-- END navbar header-->
         <!-- START Nav wrapper-->
         <div class="nav-wrapper">
            <!-- START Left navbar-->
            <ul class="nav navbar-nav">
               <li>
                  <a href="#" data-toggle="aside">
                     <em class="fa fa-align-left"></em>
                  </a>
               </li>
               <li>
                 
               </li>
            </ul>
            <!-- END Left navbar-->
            <!-- START Right Navbar-->
            <ul class="nav navbar-nav navbar-right">
               
          
               <!-- START User menu-->
               <li class="dropdown">
                  <a href="#" data-toggle="dropdown" data-play="bounceIn" class="dropdown-toggle">
                     <em class="fa fa-user"></em>
                  </a>
                  <!-- START Dropdown menu-->
                  <ul class="dropdown-menu">
                    
                   
                  
                     <li><a href="#">Logout</a>
                     </li>
                  </ul>
                  <!-- END Dropdown menu-->
               </li>
               <!-- END User menu-->
               <!-- START Contacts button-->
               <li>
                  <a href="#" data-toggle="offsidebar">
                     <em class="fa fa-align-right"></em>
                  </a>
               </li>
               <!-- END Contacts menu-->
            </ul>
            <!-- END Right Navbar-->
         </div>
         <!-- END Nav wrapper-->
         <!-- START Search form-->
         <form role="search" class="navbar-form">
            <div class="form-group has-feedback">
               <input type="text" placeholder="Type and hit Enter.." class="form-control">
               <div data-toggle="navbar-search-dismiss" class="fa fa-times form-control-feedback"></div>
            </div>
            
         </form>
         <!-- END Search form-->
      </nav>
      <!-- END Top Navbar-->
      <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <nav class="sidebar">
            <ul class="nav">
               <!-- START user info-->
               <li>
                  <div data-toggle="collapse-next" class="item user-block has-submenu">
                     <!-- User picture-->
                     
                     <!-- Name and Role-->
                     <div class="user-block-info">
                        <span class="user-block-name item-text">Bienvenido, <?php echo $this->session->userdata('nombreUsuario') ?></span>
                        <span class="user-block-role"><?php echo $this->session->userdata('rol') ?></span>
                       
                       
                     </div>
                  </div>
                
               </li>
               <?php 
                  if ($this->session->userdata('rol')=='Administrador') {
                    ?>
                     <li>
              

              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-users"></em>
                
                 <span class="item-text">Adm. Usuarios</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 
                 <li>
                    <a href="<?php echo base_url();?>usuarios/listarPersonaUsuarios" title="No Sidebar" data-toggle="" class="no-submenu">
                       
                       <span class="item-text">Usuarios</span>
                    </a>
                 </li>
               
              </ul>
              <!-- END SubMenu item-->
          </li>
          <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Categoria-Tema</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Categoria/listarCategoria" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Categoria</span>
                    </a>
                 </li>
               
                 
              </ul>
              <!-- END SubMenu item-->
           </li>

          <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-tasks"></em>
                
                 <span class="item-text">Adm. Temas</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>tema/" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Nuevo Tema</span>
                    </a>
                 </li>
                 <li>
                    <a href="<?php echo base_url(); ?>Ejercicios" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Ejercicios</span>
                    </a>
                 </li>
              </ul>
              <!-- END SubMenu item-->
           </li>


           <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Diccionario</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Diccionario</span>
                    </a>
                 </li>
                 
              </ul>
              <!-- END SubMenu item-->
           </li>

          
           <?php 
            $cont = 0;
            $palabra="nada";
            $existe=false;
            ?>
          
             <li>
             <a  data-toggle="" class="">
                        
             
                        <h3><span class="item-text">Temas de Aprendizaje</span></h3>
             </a>
             </li>
            <?php
            foreach($items->result() as $row)
            {

            ?>

           <li>
          
           
            
          
            <?php 
            $existe=false;
            $nuevaPalabra = $row->NOMBRE_CATEGORIA;
            if($palabra != $nuevaPalabra)
            {
              $existe = true;
                  $palabra = $nuevaPalabra;
                  ?>
                <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                
                <em class="fa fa-book"></em>
                <span class="item-text"><?php echo $palabra; ?> </span>
               </a>
               <ul class="nav collapse ">
                 <?php 
                 $categoria = $palabra;
                 $CI = get_instance();
                 $CI ->load->model("usuario_model");
                 $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                 foreach ($result as $row2) {
                 ?> 

                  <li>
                  <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                  <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                     
                     <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                  </a>
                  </li>
                <?php
                   # code...
                 }
                 ?>
               
               
              </ul>
               <?php
               
            }
            ?>
           
             
          
           
             
              <!-- START SubMenu item-->
              
              <!-- END SubMenu item-->
          
           <?php
            }
           ?>
            </li>
           <!-- END Menu-->
                    <?php
                  }

                  if ($this->session->userdata('rol')=='Editor') {
                     ?>
                       <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Categoria-Tema</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Categoria/listarCategoria" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Categoria</span>
                    </a>
                 </li>
                
              </ul>
              <!-- END SubMenu item-->
           </li>
           
                       <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-tasks"></em>
                    
                     <span class="item-text">Adm. Temas</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>tema/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Añadir Nuevo Tema</span>
                        </a>
                     </li>
                     <li>
                    <a href="<?php echo base_url(); ?>Ejercicios" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Ejercicios</span>
                    </a>
                 </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>


               <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-sort-alpha-asc"></em>
                    
                     <span class="item-text">Diccionario</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Diccionario</span>
                        </a>
                     </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>

              
               <?php 
            $cont = 0;
            $palabra="nada";
            $existe=false;
            ?>
          
             <li>
             <a  data-toggle="" class="">
                        
             
                        <h3><span class="item-text">Temas de Aprendizaje</span></h3>
             </a>
             </li>
            <?php
                foreach($items->result() as $row)
                {

                ?>

               <li>
              
               
                
              
                <?php 
                $existe=false;
                $nuevaPalabra = $row->NOMBRE_CATEGORIA;
                if($palabra != $nuevaPalabra)
                {
                  $existe = true;
                      $palabra = $nuevaPalabra;
                      ?>
                    <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                    <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                    
                    <em class="fa fa-book"></em>
                    <span class="item-text"><?php echo $palabra; ?> </span>
                   </a>
                   <ul class="nav collapse ">
                     <?php 
                     $categoria = $palabra;
                     $CI = get_instance();
                     $CI ->load->model("usuario_model");
                     $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                     foreach ($result as $row2) {
                     ?> 

                      <li>
                      <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                      <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                         
                         <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                      </a>
                      </li>
                    <?php
                       # code...
                     }
                     ?>
                   
                   
                  </ul>
                   <?php
                   
                }
                ?>
               
                 
              
               
                 
                  <!-- START SubMenu item-->
                  
                  <!-- END SubMenu item-->
              
               <?php
                }
               ?>
                </li>
               <!-- END Menu-->
              
                     <?php
                   }

                   if ($this->session->userdata('rol')=='Estudiante') {
                     ?>
                      <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-sort-alpha-asc"></em>
                    
                     <span class="item-text">Diccionario</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Diccionario</span>
                        </a>
                     </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>

              
                <?php 
                $cont = 0;
                $palabra="nada";
                $existe=false;
                foreach($items->result() as $row)
                {

                ?>

               <li>
              
               
                
              
                <?php 
                $existe=false;
                $nuevaPalabra = $row->NOMBRE_CATEGORIA;
                if($palabra != $nuevaPalabra)
                {
                  $existe = true;
                      $palabra = $nuevaPalabra;
                      ?>
                    <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                    <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                    
                    <em class="fa fa-book"></em>
                    <span class="item-text"><?php echo $palabra; ?> </span>
                   </a>
                   <ul class="nav collapse ">
                     <?php 
                     $categoria = $palabra;
                     $CI = get_instance();
                     $CI ->load->model("usuario_model");
                     $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                     foreach ($result as $row2) {
                     ?> 

                      <li>
                      <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                      <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                         
                         <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                      </a>
                      </li>
                    <?php
                       # code...
                     }
                     ?>
                   
                   
                  </ul>
                   <?php
                   
                }
                ?>
               
                 
              
               
                 
                  <!-- START SubMenu item-->
                  
                  <!-- END SubMenu item-->
              
               <?php
                }
               ?>
                </li>
                     <?php
                   }
               ?>

          


             

               
               <!-- END Menu-->
             
            </ul>
               <!-- END user info-->
               <!-- START Menu-->
         </nav>    
      </aside>
      <!-- END aside-->
      <!-- START Main section-->
      <section>
        <section class="main-content">
        <div class="row">
              
                  <!-- START panel-->
                  <div class="panel panel-default">
                     <div class="panel-heading">Registrar Usuario</div>
                     <div class="panel-body">
                     <form method="post"  action="<?php echo base_url();?>usuarios/agregarBd">
                           <div class="form-group">
                              <label>Nombres</label>
                              <input type="text" name="nombres" placeholder="Ingresar Nombres" class="form-control" required>
                           </div>
                           <div class="form-group">
                              <label>Apellidos</label>
                              <input type="text" name="apellidos" placeholder="Ingresar Apellidos" class="form-control" required>
                           </div>
                           <div class="form-group">
                              <label>Fecha Nacimiento</label>
                              <input type="date"  name="fechaNacimiento" placeholder="Fecha Nacimiento" class="form-control" min="1960-1-1" max="2003-12-31" required>
                           </div>

                           <div class="form-group">
                              <label>Genero</label>
                              <select name="genero" class="form-control m-b">
                                 <option>Masculino</option>
                                 <option>Femenino</option>
                                
                                
                              </select>
                           </div>
                           <div class="form-group">
                              <label>Nombre de Usuario</label>
                              <input type="text" name="usuario" placeholder="Ingresar Nombre de Usuario" class="form-control" required>
                           </div>

                           <div class="form-group">
                              <label>Password</label>
                              <input type="password" name="password" placeholder="Ingresar Password" class="form-control" required>
                           </div>
                          
                           <div class="form-group">
                              <label>Rol de Usuario</label>
                              <select name="roles" class="form-control m-b" required>
                                 <option>Editor</option>
                                 <option> Administrador</option>
                                 
                                
                              </select>
                           </div>
                           <button type="submit" class="btn btn-sm btn-default">Registrar</button>
                        </form>
                     </div>
                  </div>
                  <!-- END panel-->
              
               
              
            </div>
        </section>
      <!-- END Main section-->
   </section>