<section class="wrapper">
      <!-- START Top Navbar-->
      <nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top">
         <!-- START navbar header-->
         <div class="navbar-header">
            <a href="#" class="navbar-brand">
               <div class="brand-logo">QUECHUA</div>
               <div class="brand-logo-collapsed">Q</div>
            </a>
         </div>
         <!-- END navbar header-->
         <!-- START Nav wrapper-->
         <div class="nav-wrapper">
            <!-- START Left navbar-->
            <ul class="nav navbar-nav">
               <li>
                  <a href="#" data-toggle="aside">
                     <em class="fa fa-align-left"></em>
                  </a>
               </li>
               <li>
                 
               </li>
            </ul>
            <!-- END Left navbar-->
            <!-- START Right Navbar-->
            <ul class="nav navbar-nav navbar-right">
               
          
               <!-- START User menu-->
               <li class="dropdown">
                  <a href="#" data-toggle="dropdown" data-play="bounceIn" class="dropdown-toggle">
                     <em class="fa fa-user"></em>
                  </a>
                  <!-- START Dropdown menu-->
                  <ul class="dropdown-menu">
                    
                   
                  
                  <li><a href="<?php echo base_url(); ?>usuarios/logout">Cerrar Sesion</a>
                     </li>
                  </ul>
                  <!-- END Dropdown menu-->
               </li>
               <!-- END User menu-->
               <!-- START Contacts button-->
               
            </ul>
            <!-- END Right Navbar-->
         </div>
         <!-- END Nav wrapper-->
         <!-- START Search form-->
         <form role="search" class="navbar-form">
            <div class="form-group has-feedback">
               <input type="text" placeholder="Type and hit Enter.." class="form-control">
               <div data-toggle="navbar-search-dismiss" class="fa fa-times form-control-feedback"></div>
            </div>
            
         </form>
         <!-- END Search form-->
      </nav>
      <!-- END Top Navbar-->
      <!-- START aside-->
      <aside class="aside">
         <!-- START Sidebar (left)-->
         <nav class="sidebar">
            <ul class="nav">
               <!-- START user info-->
               <li>
                  <div data-toggle="collapse-next" class="item user-block has-submenu">
                     <!-- User picture-->
                     
                     <!-- Name and Role-->
                     <div class="user-block-info">
                        <span class="user-block-name item-text">Bienvenido, <?php echo $this->session->userdata('nombreUsuario') ?></span>
                        <span class="user-block-role"><?php echo $this->session->userdata('rol') ?></span>
                       
                       
                     </div>
                  </div>
                
               </li>
               <!-- END user info-->
               <!-- START Menu-->
               <li>
               <?php 
                  if ($this->session->userdata('rol')=='Administrador') {
                    ?>
                     <li>
              

              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-users"></em>
                
                 <span class="item-text">Adm. Usuarios</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 
                 <li>
                    <a href="<?php echo base_url();?>usuarios/listarPersonaUsuarios" title="No Sidebar" data-toggle="" class="no-submenu">
                       
                       <span class="item-text">Usuarios</span>
                    </a>
                 </li>
               
              </ul>
              <!-- END SubMenu item-->
          </li>

          <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Categoria-Tema</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Categoria/listarCategoria" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Categoria</span>
                    </a>
                 </li>
               
                 
              </ul>
              <!-- END SubMenu item-->
           </li>
          <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-tasks"></em>
                
                 <span class="item-text">Adm. Temas</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>tema/" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Nuevo Tema</span>
                    </a>
                 </li>
                 <li>
                    <a href="<?php echo base_url(); ?>Ejercicios" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Ejercicios</span>
                    </a>
                 </li>
                 
              </ul>
              <!-- END SubMenu item-->
           </li>


           <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Diccionario</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Diccionario</span>
                    </a>
                 </li>
                 
              </ul>
              <!-- END SubMenu item-->
           </li>

          
           <?php 
            $cont = 0;
            $palabra="nada";
            $existe=false;
            ?>
          
             <li>
             <a  data-toggle="" class="">
                        
             
                        <h3><span class="item-text">Temas de Aprendizaje</span></h3>
             </a>
             </li>
            <?php
            foreach($items->result() as $row)
            {

            ?>

           <li>
          
           
            
          
            <?php 
            $existe=false;
            $nuevaPalabra = $row->NOMBRE_CATEGORIA;
            if($palabra != $nuevaPalabra)
            {
              $existe = true;
                  $palabra = $nuevaPalabra;
                  ?>
                <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                
                <em class="fa fa-book"></em>
                <span class="item-text"><?php echo $palabra; ?> </span>
               </a>
               <ul class="nav collapse ">
                 <?php 
                 $categoria = $palabra;
                 $CI = get_instance();
                 $CI ->load->model("usuario_model");
                 $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                 foreach ($result as $row2) {
                 ?> 

                  <li>
                  <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                  <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                     
                     <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                  </a>
                  </li>
                <?php
                   # code...
                 }
                 ?>
               
               
              </ul>
               <?php
               
            }
            ?>
           
             
          
           
             
              <!-- START SubMenu item-->
              
              <!-- END SubMenu item-->
          
           <?php
            }
           ?>
            </li>
           <!-- END Menu-->
                    <?php
                  }

                  if ($this->session->userdata('rol')=='Editor') {
                     ?>
                       <li>
              <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                 <em class="fa fa-sort-alpha-asc"></em>
                
                 <span class="item-text">Categoria-Tema</span>
              </a>
              <!-- START SubMenu item-->
              <ul class="nav collapse ">
                 <li>
                    <a href="<?php echo base_url(); ?>Categoria/listarCategoria" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Categoria</span>
                    </a>
                 </li>
               
                 
              </ul>
              <!-- END SubMenu item-->
           </li>
                       <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-tasks"></em>
                    
                     <span class="item-text">Adm. Temas</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>tema/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Añadir Nuevo Tema</span>
                        </a>
                     </li>

                     <li>
                    <a href="<?php echo base_url(); ?>Ejercicios" title="Default" data-toggle="" class="no-submenu">
                       <span class="item-text">Añadir Ejercicios</span>
                    </a>
                 </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>


               <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-sort-alpha-asc"></em>
                    
                     <span class="item-text">Diccionario</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Diccionario</span>
                        </a>
                     </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>

              
               <?php 
            $cont = 0;
            $palabra="nada";
            $existe=false;
            ?>
          
             <li>
             <a  data-toggle="" class="">
                        
             
                        <h3><span class="item-text">Temas de Aprendizaje</span></h3>
             </a>
             </li>
            <?php
                foreach($items->result() as $row)
                {

                ?>

               <li>
              
               
                
              
                <?php 
                $existe=false;
                $nuevaPalabra = $row->NOMBRE_CATEGORIA;
                if($palabra != $nuevaPalabra)
                {
                  $existe = true;
                      $palabra = $nuevaPalabra;
                      ?>
                    <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                    <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                    
                    <em class="fa fa-book"></em>
                    <span class="item-text"><?php echo $palabra; ?> </span>
                   </a>
                   <ul class="nav collapse ">
                     <?php 
                     $categoria = $palabra;
                     $CI = get_instance();
                     $CI ->load->model("usuario_model");
                     $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                     foreach ($result as $row2) {
                     ?> 

                      <li>
                      <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                      <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                         
                         <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                      </a>
                      </li>
                    <?php
                       # code...
                     }
                     ?>
                   
                   
                  </ul>
                   <?php
                   
                }
                ?>
               
                 
              
               
                 
                  <!-- START SubMenu item-->
                  
                  <!-- END SubMenu item-->
              
               <?php
                }
               ?>
                </li>
               <!-- END Menu-->
              
                     <?php
                   }

                   if ($this->session->userdata('rol')=='Estudiante') {
                     ?>
                      <li>
                  <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                     <em class="fa fa-sort-alpha-asc"></em>
                    
                     <span class="item-text">Diccionario</span>
                  </a>
                  <!-- START SubMenu item-->
                  <ul class="nav collapse ">
                     <li>
                        <a href="<?php echo base_url(); ?>Diccionario/" title="Default" data-toggle="" class="no-submenu">
                           <span class="item-text">Diccionario</span>
                        </a>
                     </li>
                     
                  </ul>
                  <!-- END SubMenu item-->
               </li>

              
                <?php 
                $cont = 0;
                $palabra="nada";
                $existe=false;
                foreach($items->result() as $row)
                {

                ?>

               <li>
              
               
                
              
                <?php 
                $existe=false;
                $nuevaPalabra = $row->NOMBRE_CATEGORIA;
                if($palabra != $nuevaPalabra)
                {
                  $existe = true;
                      $palabra = $nuevaPalabra;
                      ?>
                    <input type="hidden" name="idcategoria" value="<?php echo $row->IDCATEGORIA;?>">
                    <a href="" title="Dashboard" data-toggle="collapse-next" class="has-submenu">
                    
                    <em class="fa fa-book"></em>
                    <span class="item-text"><?php echo $palabra; ?> </span>
                   </a>
                   <ul class="nav collapse ">
                     <?php 
                     $categoria = $palabra;
                     $CI = get_instance();
                     $CI ->load->model("usuario_model");
                     $result = $CI->tema_model->listarItemsWhere($categoria)->result_array();
                     foreach ($result as $row2) {
                     ?> 

                      <li>
                      <input type="hidden" name="idtema" value="<?php echo $row2['IDTEMA'];?>">
                      <a href="<?php echo base_url();?>tema/VerTema/<?php echo $row2['IDTEMA'];?>" title="No Sidebar" data-toggle="" class="no-submenu">
                         
                         <span class="item-text"><?php echo $row2['TITULO']; ?></span>
                      </a>
                      </li>
                    <?php
                       # code...
                     }
                     ?>
                   
                   
                  </ul>
                   <?php
                   
                }
                ?>
               
                 
              
               
                 
                  <!-- START SubMenu item-->
                  
                  <!-- END SubMenu item-->
              
               <?php
                }
               ?>
                </li>
                     <?php
                   }
               ?>

          

               </li>   
            </ul>
         </nav>
         <!-- END Off Sidebar (right)-->
      </aside>
      <!-- END aside-->
      <!-- START Main section-->
      <section>
        <section class="main-content">
            <h3>Lista de Usuarios  
              
              
            </h3>

            <div class="pull-right">
   <a href="<?php echo base_url(); ?>usuarios/agregar">
      <button type="button" class="btn btn-labeled btn-success">
                           <span class="btn-label"><i class="fa fa-plus"></i>
                           </span>Agregar</button></a>

                        </div>
            
            
            <!-- START DATATABLE 1 -->
            <div class="row">
               <div class="col-lg-12">
                  <div class="panel panel-default">
                     <div class="panel-heading">Usuarios
                        
                     </div>
                     <div class="panel-body">
                        <table id="datatable1" class="table table-striped table-hover">
                           <thead>
                              <tr>
                              <th>Noº</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Genero</th>
                            <th>Nombre Usuario</th>
                            <th>Rol</th>
                            <th>Estado</th>
                            <th>Modificar</th>
                            <th>Eliminar</th>
                            
                              </tr>
                           </thead>
                           <tbody>                           
                           <?php
$indice=1;
foreach ($usuarios->result() as $row)
{
  
?>
                              <tr>
                              <input type="hidden" name="idusuario" value="<?php echo $row->id_Usuario;?>">
                                 <td><?php echo $indice;?></td>     
                                 <td><?php echo $row->nombres; ?></td>
                                 <td><?php echo $row->apellidos; ?></td>
                                 <td><?php 
                                 if ($row->genero==1) {
                                    echo "Masculino";
                                 }
                                 else
                                 {
                                    echo "Femenino";
                                 }
                                 ?></td>
                                 <td><?php echo $row->nombreUsuario; ?></td>
                                 <td><?php echo $row->rol; ?></td>
                                 <td>
                                 <?php 
                                 if ($row->estado == 1) {
                                    ?>
                                    <?php echo form_open_multipart('usuarios/activar_inactivar'); ?>
                                    <input type="hidden" name="idusuario" value="<?php echo $row->id_Usuario;?>">
                                    <input type="hidden" name="control" value="Activo">
                                    <button type="submit" class="btn btn-danger"><?php echo "Desactivar"; ?> </button>
                                    <?php echo form_close(); ?>
                                 

                                    <?php
                                 }
                                 else
                                 {
                                    ?>
                                    <?php echo form_open_multipart('usuarios/activar_inactivar'); ?>
                                    <input type="hidden" name="idusuario" value="<?php echo $row->id_Usuario;?>">
                                    <input type="hidden" name="control" value="Inactivo">
                                    <button type="submit" class="btn btn-success"><?php echo "Activar"; ?> </button>
                                    <?php echo form_close(); ?>
                                    <?php
                                 }
                                 
                                 
                                 ?>
                                 </td>
                                 <td>
<?php echo form_open_multipart('usuarios/modificar'); ?>
<input type="hidden" name="idusuario" value="<?php echo $row->id_Usuario;?>">
<button type="submit" class="btn btn-primary">Modificar</button>
<?php echo form_close(); ?>
                              </td>
                                 <td>

<?php echo form_open_multipart('usuarios/eliminarbd'); ?>
<input type="hidden" name="idusuario" value="<?php echo $row->id_Usuario;?>">
<button type="submit" class="btn btn-primary">Eliminar</button>
<?php echo form_close(); ?>
                                 </td>
                              </tr>
<?php
$indice++;
}
?>




                            

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
        </section>
</section>
      <!-- END Main section-->
   </section>